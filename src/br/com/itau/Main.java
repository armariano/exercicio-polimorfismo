package br.com.itau;

public class Main {

    public static void main(String[] args) {

	    Funcionario funcionario = new Funcionario("alderri", "987284073");


	    funcionario.setNome("Alder");
	    funcionario.setIdade(30);
	    funcionario.setSexo("M");

        Pessoa pessoa = new Pessoa("Pedrinho", 12, "M");

        System.out.println(funcionario.getRacf());
        System.out.println(funcionario.getFuncional());
        System.out.println(funcionario.getNome());
        System.out.println(funcionario.getIdade());
        System.out.println(funcionario.getSexo());

        funcionario.exibirMensagem("teste");
        pessoa.exibirMensagem("teste");
    }
}
