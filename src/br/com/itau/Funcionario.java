package br.com.itau;

public class Funcionario extends Pessoa {
    protected  String racf;
    protected  String funcional;

    public Funcionario(String racf, String funcional) {
        this.racf = racf;
        this.funcional = funcional;
    }

    public Funcionario(){}

    public String getRacf() {
        return racf;
    }

    public void setRacf(String racf) {
        this.racf = racf;
    }

    public String getFuncional() {
        return funcional;
    }

    public void setFuncional(String funcional) {
        this.funcional = funcional;
    }

    @Override
    public void exibirMensagem(String msg){
        System.out.println("Classe Funcionario - " + msg);
    }
}
